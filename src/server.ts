import 'reflect-metadata';
import * as dotenv from "dotenv";
dotenv.config({ path: ".env" });
import { App } from './app';
import { PostgresDBConnection } from './postgresDBConnection';
import { Answer } from "./models/Answer";
import { Poll } from "./models/Poll";
import { Trivia } from "./models/Trivia";
import { VotableAnswer } from "./models/VotableAnswer";
import { Question } from "./models/Question";
import PollController from './controllers/pollController';
import { Container } from 'typeorm-typedi-extensions';
import TriviaController from './controllers/triviaController';

const startServer = async () => {
    const dbConnection = new PostgresDBConnection();
    await dbConnection.dbCreateConnection([Answer, VotableAnswer, Poll, Trivia, Question]);
    const app = new App([Container.get(PollController), Container.get(TriviaController)], 3000);
    app.listen();
}

startServer();

