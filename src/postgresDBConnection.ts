import { Connection, createConnection, useContainer } from "typeorm";
import { getTypeOrmConfig } from "./ormconfig";
import { Container } from 'typedi';

export class PostgresDBConnection {
    
    async dbCreateConnection(models: any[]) {
     let conn: Connection;
      try {
        useContainer(Container);
        const config = getTypeOrmConfig(models);
        conn = await createConnection(config);
        console.log(`Database connection success. Connection name: '${conn.name}' Database: '${conn.options.database}'`);
        return { config, conn };
      } catch (error) {
      console.log(`error :>> '${error}'`);
    }
  };
}