import { ConnectionOptions } from "typeorm";
import "reflect-metadata";

const getModels = (Models: any[]) => {
  const models: string[] = [];
  for (const key in Models) {
    models.push((<any>Models)[key]);
  }
  return models;
};

export const getTypeOrmConfig = (models: any[]) => {
  return <ConnectionOptions> {
    type: "postgres",
    host: process.env.POSTGRES_HOST,
    port: Number(process.env.POSTGRES_PORT),
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB_NAME,    
    entities: getModels(models),
    logging: process.env.ENABLE_POSTGRES_LOGS === "true" || false,
    synchronize: true
 };
}
