import { ChildEntity, Entity, OneToMany } from "typeorm";
import { Answer } from "./Answer";
import { Question } from "./Question";

@Entity()
@ChildEntity()
export abstract class MultiAnswerQuestion extends Question {
    @OneToMany(() => Answer, answer => answer.ownerQuestion, {cascade: true})
    answers: Answer[];
}