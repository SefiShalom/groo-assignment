import { Column, Entity, PrimaryGeneratedColumn, TableInheritance } from "typeorm";

@Entity()
@TableInheritance({column: {type: 'varchar', name: 'type'}})
export abstract class Question {
    
    @PrimaryGeneratedColumn({ type: "integer"})
    id: number;
    
    @Column("text", { nullable: false })
    questionText!: string;
}