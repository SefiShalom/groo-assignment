import { ChildEntity, Entity, JoinColumn, OneToOne, TableInheritance } from "typeorm";
import { Answer } from "./Answer";
import { Question } from "./Question";

@Entity()
@ChildEntity()
export abstract class SingleAnswerQuestion extends Question {
    
    @OneToOne(() => Answer, answer => answer.ownerQuestion)
    answer: Answer;

}