import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, TableInheritance } from "typeorm";
import { Question } from "./Question";

@Entity()
@TableInheritance({column: {type: 'varchar', name: 'type'}})
export class Answer {
    
    @PrimaryGeneratedColumn({ type: "integer", name: "id" })
    id: number;

    @ManyToOne(() => Question)
    ownerQuestion: Question;

    @Column("text", { nullable: true })
    answerText!: string;
    
}