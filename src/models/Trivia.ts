import { ChildEntity, JoinColumn, OneToOne } from "typeorm";
import { MultiAnswerQuestion } from "./MultiAnswerQuestion";
import { VotableAnswer } from "./VotableAnswer";

@ChildEntity()
export class Trivia extends MultiAnswerQuestion {
    
    @OneToOne(() => VotableAnswer, { nullable: true, cascade: true })
    @JoinColumn()
    correctAnswer: VotableAnswer;
}