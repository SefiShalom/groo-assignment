import { ChildEntity, Column, Entity } from "typeorm";
import { Answer } from "./Answer";

@ChildEntity()
export class VotableAnswer extends Answer {
    @Column("integer", { default: 0 })
    numberOfVotes!: number;
}