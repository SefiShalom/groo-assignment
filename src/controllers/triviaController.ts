import "reflect-metadata";
import express from "express";
import { Repository } from "typeorm";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Trivia } from "../models/Trivia";
import { VotableAnswer } from '../models/VotableAnswer';
import { Service } from "typedi";

@Service()
export default class TriviaController {
  public path = '/trivia';
  public router = express.Router();
 
  constructor(
    @InjectRepository(Trivia) private readonly triviaRepository: Repository<Trivia>,
    @InjectRepository(VotableAnswer) private readonly votableAnswerRepository: Repository<VotableAnswer>
  ) {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.get(`${this.path}/get/:id`, this.get.bind(this));
    this.router.post(`${this.path}/insert`, this.insert.bind(this));
    this.router.put(`${this.path}/vote/:id/:answerId`, this.vote.bind(this));
  }
 
  async get(request: express.Request, response: express.Response) {
    try {
      const triviaId = request.params.id as string;
      if(!triviaId) {
        return response.status(400).send("trivia was not provided");
      }
      const trivia = await this.triviaRepository.findOne(triviaId, {relations: ["answers", "correctAnswer"]});
      response.status(200).send(trivia || "Trivia was not found");
    } catch(err: any) {
      response.status(500).send(JSON.stringify(err));
    }
  }
 
  async insert(request: express.Request, response: express.Response) {
    try {
      const questionText = request.body["question"];
      const answers = request.body["answers"];
      const correctAnswerIndex = parseInt(request.body["correctAnswerIndex"]);

      if(!questionText || questionText.length === 0) {
        return response.status(400).send("Question must not be empty!");
      } else if(!answers || answers.length === 0) {
        return response.status(400).send("Answers must not be empty!");
      } else if(correctAnswerIndex === undefined || NaN) {
        return response.status(400).send("correctAnswerIndex must not be empty!");
      }

      const triviaToInsert: Trivia = new Trivia();
      const answersToInsert: VotableAnswer[] = [];

      for(const answer of answers) {
        const answerToInsert = new VotableAnswer();
        Object.assign(answerToInsert, { answerText: answer });
        answersToInsert.push(answerToInsert);
      }
      Object.assign(triviaToInsert, { questionText }, { answers: answersToInsert }, { correctAnswer: answersToInsert[correctAnswerIndex] });
      await this.triviaRepository.save(triviaToInsert);
      return response.status(200).send({ triviaId: triviaToInsert.id });
    } catch(err: any) {
      response.status(500).send(JSON.stringify(err));
    }
  }

  async vote(request: express.Request, response: express.Response) {
    try {
      const triviaId = request.params.id;
      const answerId = parseInt(request.params.answerId);
      if(!answerId) {
        return response.status(400).send("answerId was not provided");
      }else if(!triviaId) {
        return response.status(400).send("triviaId was not provided");
      }
      const trivia = await this.triviaRepository.findOne(triviaId, {relations: ["answers"]});
      if(!trivia) {
        return response.status(200).send("Trivia was not found");
      }
      const votedAnswer = trivia.answers.find(ans => ans.id === answerId) as VotableAnswer;
      if(!votedAnswer){
        return response.status(400).send(`Answer ${answerId} was not found in Trivia ${triviaId}`);
      }
      votedAnswer.numberOfVotes++;
      await this.votableAnswerRepository.save(votedAnswer);
      const triviaAnswers: VotableAnswer[] = trivia.answers.sort((ans1: any, ans2: any) => ans2.numberOfVotes - ans1.numberOfVotes) as VotableAnswer[];
      const votesPerAnswer: any = {};
      triviaAnswers.forEach((ans: VotableAnswer) => votesPerAnswer[ans.answerText] = ans.numberOfVotes);
      return response.status(200).send(votesPerAnswer);
    } catch(err: any) {
      response.status(500).send(JSON.stringify(err));
    }
  }

};