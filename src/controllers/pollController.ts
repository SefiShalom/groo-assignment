import express from "express";
import { Repository } from "typeorm";
import { Poll } from "../models/Poll";
import { VotableAnswer } from '../models/VotableAnswer';
import { InjectRepository } from "typeorm-typedi-extensions";
import { Service } from "typedi";

@Service()
export default class PollController {

  public path = '/polls';
  public router = express.Router();

  constructor(
    @InjectRepository(Poll) private readonly pollRepository: Repository<Poll>,
    @InjectRepository(VotableAnswer) private readonly votableAnswerRepository: Repository<VotableAnswer>
  ) {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.get(`${this.path}/get/:id`, this.get.bind(this));
    this.router.post(`${this.path}/insert`, this.insert.bind(this));
    this.router.put(`${this.path}/vote/:id/:answerId`, this.vote.bind(this));
  }
 
  async get(request: express.Request, response: express.Response) {
    try {
      const pollId = request.params.id as string;
      if(!pollId) {
        return response.status(400).send("pollId was not provided");
      }
      const poll = await this.pollRepository.findOne(pollId, {relations: ["answers"]});
      return response.status(200).send(poll || "Poll was not found");
    } catch(err: any) {
      response.status(500).send(JSON.stringify(err));
    }
  }
 
  async insert(request: express.Request, response: express.Response) {
    try {
      const questionText = request.body["question"] as string;
      const answers = request.body["answers"] as string | string[];

      if(!questionText || questionText.length === 0) {
        return response.status(400).send("Question must not be empty!");
      } else if(!answers || answers.length === 0) {
        return response.status(400).send("Answers must not be empty!");
      }

      const pollToInsert: Poll = new Poll();
      const answersToInsert: VotableAnswer[] = [];
      
      for(const answer of answers) {
        const answerToInsert = new VotableAnswer();
        Object.assign(answerToInsert, {answerText: answer});
        answersToInsert.push(answerToInsert);
      }
      Object.assign(pollToInsert, { questionText }, { answers: answersToInsert });
      await this.pollRepository.save(pollToInsert);
      return response.status(200).send({ pollId: pollToInsert.id });
    } catch(err: any) {
      response.status(500).send(JSON.stringify(err));
    }
  }

  async vote(request: express.Request, response: express.Response) {
    try {
      const pollId = request.params.id;
      const answerId = parseInt(request.params.answerId);
      if(!answerId) {
        return response.status(400).send("answerId was not provided");
      }else if(!pollId) {
        return response.status(400).send("pollId was not provided");
      }
      const poll = await this.pollRepository.findOne(pollId, {relations: ["answers"]});
      if(!poll) {
        return response.status(200).send("Poll was not found");
      }
      const votedAnswer = poll.answers.find(ans => ans.id === answerId) as VotableAnswer;
      if(!votedAnswer){
        return response.status(400).send(`Answer ${answerId} was not found in Poll ${pollId}`);
      }
      votedAnswer.numberOfVotes++;
      await this.votableAnswerRepository.save(votedAnswer);
      const pollAnswers: VotableAnswer[] = poll.answers.sort((ans1: any, ans2: any) => ans2.numberOfVotes - ans1.numberOfVotes) as VotableAnswer[];
      const votesPerAnswer: any = {};
      pollAnswers.forEach((ans: VotableAnswer) => votesPerAnswer[ans.answerText] = ans.numberOfVotes);
      return response.status(200).send(votesPerAnswer);
    } catch(err: any) {
      response.status(500).send(JSON.stringify(err));
    }
  }

};